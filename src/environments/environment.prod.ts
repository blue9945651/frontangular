export const environment = {
    production: true,
    // endpoint: 'https://localhost:44391/blue/', //Entity
    endpoint: 'https://localhost:44382/blue/', //Dapper

    pTituloError: 'Ops...',
    pTituloSucess: "Ok!",
    pTituloInfo: "Atenção",
    pTituloQuestion: "Deseja realmente excluir este registro ?",

    pMensagemLoggof: 'Por medidas de segurança você foi desconectado de nossos servidores. Por favor, desconecte do sistema e conecte novamente. Caso o erro persista entre em contato com o administrador do sistema',
    pMensagemUnexpected: 'Ocorreu erro inesperado ao acesso recurso do servidor. Tente desconectar e reconectar do sistema. Caso o erro persista entre em contato com o administradpr do sistema',

    pMensagemPerdaDados: 'Deseja realmente sair desta opção e perder as alterações realizadas ?'
};

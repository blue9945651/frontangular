import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { AppLayoutComponent } from "./layout/app.layout.component";

@NgModule({
    imports: [
        RouterModule.forRoot([
            {
                path: '', component: AppLayoutComponent,
                children: [
                    {path: '', loadChildren: () => import('./layout/pages/default/default.module').then(m => m.DefaultModule)},
                    {path: 'agendas', loadChildren: () => import('./layout/pages/agenda/agenda.module').then(m => m.AgendaModule)},
                ]
            },
            { path: 'login', loadChildren: () => import('./layout/pages/auth/login/login.module').then(m => m.LoginModule) },
            {path: '**', redirectTo: '/notfound'}
        ], { scrollPositionRestoration: 'enabled', anchorScrolling: 'enabled', onSameUrlNavigation: 'reload' })
    ],
    exports: [RouterModule]
})
export class AppRoutingModule {
}

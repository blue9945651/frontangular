import { Injectable } from "@angular/core";
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { Observable, throwError } from "rxjs";
import { catchError, finalize } from "rxjs/operators";
import { Router } from "@angular/router";
import { LoaderService } from "./loaderService";
import { environment } from "src/environments/environment";
import { ShowErrorMessage, ShowSuccessMessageWait } from "./general";
import { LogBreak } from "./log.breaker";

@Injectable()
export class HttpInterceptorService implements HttpInterceptor {
    private requests: HttpRequest<any>[] = [];

    constructor(public router: Router, private loaderService: LoaderService) { }

    intercept (
        req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
            if (!req.url.includes('Usuarios/Ping') && !req.url.includes('Notificacoes/GetAll'))
                this.loaderService.isLoading.next(true);

            return next.handle(req).pipe(
            catchError(error => {
                if ([401, 403].includes(error.status) && localStorage.getItem('nome') !== null && localStorage.getItem('undefined')) {
                    ShowSuccessMessageWait(environment.pMensagemLoggof).then((result) => {
                        sessionStorage.clear();
                        this.router.navigate(['/login']);
                    });
                }
                else {
                    let mensagem: string = LogBreak(error);
                    ShowErrorMessage(mensagem);
                }

                return throwError(error);
            }),
            finalize(() => {
                if (!req.url.includes('Usuarios/Ping') && !req.url.includes('Notificacoes/GetAll'))
                    this.loaderService.isLoading.next(this.requests.length > 0);
            })
        );
    }
}

import { HttpHeaders } from "@angular/common/http";
import { MessageService } from "primeng/api";
import { environment } from "src/environments/environment";
import Swal from "sweetalert2";

export function MountHeader() {
    let cheaders = new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${sessionStorage.getItem('token')}`,
        'Anchor': `${sessionStorage.getItem('anchor')}`
    });
    return cheaders;
}

export function ShowErrorMessage(message: string) {
    Swal.fire({ icon: 'error', title: environment.pTituloError, html: message, customClass: { container: 'swal-Alert' } });
}

export function ShowErrorMessageWait(message: string) {
    return Swal.fire({ icon: 'error', title: environment.pTituloError, html: message, customClass: { container: 'swal-Alert' } });
}

export function ShowSuccessMessage(message: string) {
    Swal.fire({ icon: 'success', title: environment.pTituloSucess, html: message, customClass: { container: 'swal-Alert' } });
}

export function ShowSuccessMessageWait(message: string) {
    return Swal.fire({ icon: 'success', title: (message === undefined || message === null || message === '' ? environment.pTituloSucess : message), confirmButtonText: 'Ok', showDenyButton: false });
}

export function ShowInfoMessage(message: string) {
    Swal.fire({ icon: 'info', title: environment.pTituloInfo, html: message, customClass: { container: 'swal-Alert' } });
}

export function ShowInfoMessageWait(message: string) {
    return Swal.fire({ icon: 'info', title: environment.pTituloInfo, html: message, customClass: { container: 'swal-Alert' } });
}

export function ShowConfirmMessage(message?: string) {
    return Swal.fire({ icon: 'question', title: (message === undefined || message === null || message === '' ? environment.pTituloQuestion : message), customClass: { container: 'swal-Alert' }, confirmButtonText: 'Sim, eu confirmo !', showDenyButton: true, denyButtonText: 'Desisto' });
}

export function SendMessage(messageService: MessageService, pseverity: string, message: string) {
    messageService.add({ severity: pseverity, summary: 'Ok!', detail: message, life: 3000 });
}

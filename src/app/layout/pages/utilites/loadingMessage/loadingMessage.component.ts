import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'LoadingMessageComponent',
    templateUrl: './loadingMessage.component.html'
})
export class LoadingMessageComponent implements OnInit {
    display: boolean = false;
    loadingMessage: string = "aguarde";
    loadingMessageWaiting: string = "";

    ngOnInit() {

    }

    show(message?: string, warning?: string) {
        this.loadingMessage = message == null ? "aguarde" : message;
        this.loadingMessageWaiting = warning == null ? '' : warning;
        this.display = true;
    }

    dispose() {
        this.display = false;
    }
}

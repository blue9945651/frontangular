import { BehaviorSubject } from "rxjs";

export class LoaderService {
    public isLoading = new BehaviorSubject(false);
    constructor() { }
}

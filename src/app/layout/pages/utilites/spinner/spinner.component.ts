import { Component, OnInit } from '@angular/core';
import { LoaderService } from '../loaderService';

@Component({
    selector: 'SpinnerComponent',
    templateUrl: './spinner.component.html'
})
export class SpinnerComponent implements OnInit {
    loading: boolean;

    constructor(private loaderService: LoaderService) {
        this.loaderService.isLoading.subscribe((v) => {
            this.loading = v;
        });
    }

    ngOnInit() {

    }
}

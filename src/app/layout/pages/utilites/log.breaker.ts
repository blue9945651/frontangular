import { HttpErrorResponse } from '@angular/common/http';

export function LogBreak(error: HttpErrorResponse): any
{
    let messageError: string = "";

    for (let index = 0; index < 20; index++) {
        try {
            if (error.error.Errors[Object.keys(error.error.Errors)[index]] != undefined)
                messageError += ' - ' + error.error.Errors[Object.keys(error.error.Errors)[index]] + '<br />';
            else
                break;
        }
        catch {
            try
            {
                if (error.error.type === undefined)
                    messageError = error.error;
                else
                    messageError = error.message;
            }
            catch
            {
                messageError = error.message;
            }
        }
    }

    return messageError;
}

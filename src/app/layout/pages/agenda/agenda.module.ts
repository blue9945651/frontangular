import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonModule } from 'primeng/button';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InputTextModule } from 'primeng/inputtext';
import { HttpClientModule } from '@angular/common/http';
import { AppConfigModule } from '../../config/config.module';
import { RippleModule } from 'primeng/ripple';
import { AutoFocusModule } from 'primeng/autofocus';
import { AgendaRoutingModule } from './agenda-routing.module';
import { AgendaComponent } from './agenda.component';
import { ToolbarModule } from 'primeng/toolbar';
import { ToastModule } from 'primeng/toast';
import { TableModule } from 'primeng/table';
import { DialogModule } from 'primeng/dialog';
import { AgendaPopupComponent } from './agenda.popup.component';

@NgModule({
    imports: [
        HttpClientModule,
        AppConfigModule,
        RippleModule,
        FormsModule,
        CommonModule,
        ButtonModule,
        ReactiveFormsModule,
        AutoFocusModule,
        InputTextModule,
        ToolbarModule,
        ToastModule,
        TableModule,
        DialogModule,
        AgendaRoutingModule
    ],
    declarations: [AgendaComponent, AgendaPopupComponent]
})
export class AgendaModule { }

import { Component, EventEmitter, OnInit, Output, inject } from '@angular/core';
import { IAgenda } from './agenda.interfaces';
import { AgendaService } from './agenda.services';
import { MessageService } from 'primeng/api';
import { SendMessage, ShowConfirmMessage } from '../utilites/general';
import { environment } from 'src/environments/environment';

@Component({
    selector: 'AgendaPopupComponent',
    templateUrl: './agenda.popup.component.html',
    providers: [MessageService],
    styles: [`
        :host ::ng-deep .pi-eye,
        :host ::ng-deep .pi-eye-slash {
            transform:scale(1.6);
            margin-right: 1rem;
            color: var(--primary-color) !important;
        }
    `]
})
export class AgendaPopupComponent implements OnInit {
    @Output("loadGrid") loadGrid: EventEmitter<any> = new EventEmitter();

    messageService = inject(MessageService);
    agendaService = inject(AgendaService);

    modalDialog: boolean = false;
    blnNgModelChanged: boolean = false;
    newdata: boolean = false;

    agenda: IAgenda = {};

    ngOnInit() {

    }

    show(id: number) {
        if (id === null)
            this.openNew()
        else
            this.loadAndOpen(id);
    }

    openNew() {
        this.agenda = {} = {};
        this.blnNgModelChanged = false;
        this.newdata = true;
        this.modalDialog = true;
    }

    loadAndOpen(id: number) {
        this.agendaService.get(id).subscribe((resp: IAgenda) => {
            this.agenda = resp;
            console.log(this.agenda);
            this.openModal();
        });
    }

    openModal() {
        this.blnNgModelChanged = false;
        this.newdata = false;
        this.modalDialog = true;
    }

    saveData() {
        if (this.newdata) {
            this.agenda.LoginId = parseInt(sessionStorage.getItem('id'));
            this.agendaService.create(this.agenda).subscribe((resp: any) => {
                this.modalDialog = false;
                SendMessage(this.messageService, 'success', 'Dados criados com sucesso');
                this.emitter();
            });
        }
        else {
            this.agendaService.save(this.agenda).subscribe((resp: any) => {
                this.modalDialog = false;
                SendMessage(this.messageService, 'success', 'Dados alterados com sucesso');
                this.emitter();
            });
        }
    }

    emitter() {
        this.loadGrid.emit();
    }

    onChange(newValue) {
        this.blnNgModelChanged = true;
    }

    hideDialog() {
        if (this.blnNgModelChanged) {
            ShowConfirmMessage(environment.pMensagemPerdaDados).then((result)=> {
                if (result.value)
                    this.modalDialog = false;
            });
        }
        else
            this.modalDialog = false;
    }
}

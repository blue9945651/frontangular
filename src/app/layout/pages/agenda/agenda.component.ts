import { Component, ElementRef, OnInit, ViewChild, inject } from '@angular/core';
import { Router } from '@angular/router';
import { LayoutService } from 'src/app/layout/service/app.layout.service';
import { IAgenda } from './agenda.interfaces';
import { AgendaService } from './agenda.services';
import { MessageService } from 'primeng/api';
import { AgendaPopupComponent } from './agenda.popup.component';
import { Table } from 'primeng/table';
import { SendMessage, ShowConfirmMessage } from '../utilites/general';

@Component({
    selector: 'AgendaComponent',
    templateUrl: './agenda.component.html',
    providers: [MessageService],
    styles: [`
        :host ::ng-deep .pi-eye,
        :host ::ng-deep .pi-eye-slash {
            transform:scale(1.6);
            margin-right: 1rem;
            color: var(--primary-color) !important;
        }
    `]
})
export class AgendaComponent implements OnInit {
    layoutService = inject(LayoutService);
    router = inject(Router);
    agendaService = inject(AgendaService);
    messageService = inject(MessageService);

    agendas: IAgenda[] = [];

    @ViewChild('dt') table: Table;
    @ViewChild('filter') filter: ElementRef;
    rowGroupMetadata: any | undefined;

    @ViewChild('AgendaPopupComponent', { static: true }) AgendaPopupComponent: AgendaPopupComponent;

    ngOnInit() {
        if (sessionStorage.getItem('nome') == null) {
            this.router.navigate(['/login']);
        }
    }

    ngAfterContentInit() {
        this.loadGrid();
    }

    loadGrid() {
        this.agendaService.getAll(parseInt(sessionStorage.getItem('id'))).subscribe((resp: IAgenda[]) => {
            this.agendas = resp;
        });
    }

    openNew() {
        this.AgendaPopupComponent.show(null);
    }

    loadAndOpen(id: number) {
        this.AgendaPopupComponent.show(id);
    }

    deleteData(id: number) {
        ShowConfirmMessage().then((result) => {
            if (result.value) {
                this.agendaService.delete(id).subscribe((resp: any) => {
                    SendMessage(this.messageService, 'success', 'Registro excluído com sucesso');
                    this.loadGrid();
                });
            }
        });
    }

    clear(table: Table) {
        table.clear();
        this.filter.nativeElement.value = '';
    }
}

export interface IAgenda {
    Id?: number;
    LoginId?: number;
    Nome?: string;
    Email?: string;
    Telefone?: string;
}

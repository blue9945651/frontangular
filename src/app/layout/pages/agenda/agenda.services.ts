import { Injectable, OnDestroy, inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject, takeUntil } from 'rxjs';
import { environment } from 'src/environments/environment';
import { IAgenda } from './agenda.interfaces';
import { MountHeader } from '../utilites/general';

@Injectable({
  providedIn: 'root'
})
export class AgendaService implements OnDestroy {
    url: string = "";
    http = inject(HttpClient);
    ngDestroy$ = new Subject();

    getAll(ID: number): Observable<IAgenda[]> {
        this.url = `${environment.endpoint}Agenda/GetAll?ID=${ID}`;
        return this.http.get<IAgenda[]>(this.url, { headers: MountHeader() }).pipe(takeUntil(this.ngDestroy$));
    }

    get(ID: number): Observable<IAgenda> {
        this.url = `${environment.endpoint}Agenda/Get?ID=${ID}`;
        return this.http.get<IAgenda>(this.url, { headers: MountHeader() }).pipe(takeUntil(this.ngDestroy$));
    }

    create(agenda: IAgenda): Observable<any> {
        this.url = `${environment.endpoint}Agenda/Create`;
        return this.http.post<any>(this.url, agenda, { headers: MountHeader() }).pipe(takeUntil(this.ngDestroy$));
    }

    save(agenda: IAgenda): Observable<any> {
        this.url = `${environment.endpoint}Agenda/Update`;
        return this.http.put<any>(this.url, agenda, { headers: MountHeader() }).pipe(takeUntil(this.ngDestroy$));
    }

    delete(id: number): Observable<any> {
        this.url = `${environment.endpoint}Agenda/Delete?ID=${id}`;
        return this.http.delete<any>(this.url, { headers: MountHeader() }).pipe(takeUntil(this.ngDestroy$));
    }

    ngOnDestroy(){
        this.ngDestroy$.next(true);
        this.ngDestroy$.complete();
    }
}

import { Injectable, OnDestroy, inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject, takeUntil } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ILoginIn, ILoginReturn } from './login.interfaces';

@Injectable({
  providedIn: 'root'
})
export class LoginService implements OnDestroy {
    url: string = "";
    http = inject(HttpClient);
    ngDestroy$ = new Subject();

    checkLogin(login :ILoginIn): Observable<ILoginReturn> {
        this.url = `${environment.endpoint}Login/CheckLogin`;
        return this.http.post<ILoginReturn>(this.url, login).pipe(takeUntil(this.ngDestroy$));
    }

    ngOnDestroy(){
        this.ngDestroy$.next(true);
        this.ngDestroy$.complete();
    }
}

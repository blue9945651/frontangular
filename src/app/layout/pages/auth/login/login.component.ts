import { Component, inject } from '@angular/core';
import { LayoutService } from 'src/app/layout/service/app.layout.service';
import { LoginService } from './login.services';
import { ILoginIn, ILoginReturn } from './login.interfaces';
import { ShowErrorMessage } from '../../utilites/general';
import { Router } from '@angular/router';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styles: [`
        :host ::ng-deep .pi-eye,
        :host ::ng-deep .pi-eye-slash {
            transform:scale(1.6);
            margin-right: 1rem;
            color: var(--primary-color) !important;
        }
    `]
})
export class LoginComponent {
    layoutService = inject(LayoutService);
    loginService = inject(LoginService);
    router = inject(Router);

    loginIn: ILoginIn = {};
    loginReturn: ILoginReturn = {};
    user!: string;
    password!: string;
    errorMessage?: string = "";

    Access() {
        let retorno = this.validate();

        if (!retorno) {
            ShowErrorMessage(this.errorMessage);
            return;
        }

        this.loginIn.login = this.user;
        this.loginIn.password = this.password;

        this.loginService.checkLogin(this.loginIn).subscribe((resp: ILoginReturn) => {
            if (!resp.IsSucess)
                ShowErrorMessage(resp.ErrorMessage);
            else
                this.makeLogin(resp);
        });
    }

    validate(): boolean {
        this.errorMessage = "";

        if (this.user === null || this.user === undefined)
            this.errorMessage = "Login ou Email não informado";
        else if (this.password === null || this.password === undefined)
            this.errorMessage = "Senha não informada";

        return (this.errorMessage === "");
    }

    makeLogin(loginReturn: ILoginReturn) {
        sessionStorage.setItem('id', loginReturn.ID.toString());
        sessionStorage.setItem('token', loginReturn.TOKEN);
        sessionStorage.setItem('email', loginReturn.EMAIL);
        sessionStorage.setItem('nome', loginReturn.NOME);
        this.router.navigate(['/agendas']);
    }
}

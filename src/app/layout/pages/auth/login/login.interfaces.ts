export interface ILoginIn {
    login?: string;
    password?: string;
}

export interface ILoginReturn {
    ID?: number;
    NOME?: string;
    EMAIL?: string;
    LOGIN?: string;
    TOKEN?: string;
    IsSucess?: boolean;
    ErrorMessage?: string;
}

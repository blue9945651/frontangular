import { Component, OnInit, inject } from '@angular/core';
import { Router } from '@angular/router';
import { LayoutService } from 'src/app/layout/service/app.layout.service';

@Component({
    selector: 'DefaultComponent',
    templateUrl: './default.component.html',
    styles: [`
        :host ::ng-deep .pi-eye,
        :host ::ng-deep .pi-eye-slash {
            transform:scale(1.6);
            margin-right: 1rem;
            color: var(--primary-color) !important;
        }
    `]
})
export class DefaultComponent implements OnInit {
    layoutService = inject(LayoutService);
    router = inject(Router);

    ngOnInit(): void {
        if (sessionStorage.getItem('nome') == null) {
            this.router.navigate(['/login']);
        }
    }
}

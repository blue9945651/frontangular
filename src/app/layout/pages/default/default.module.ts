import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonModule } from 'primeng/button';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InputTextModule } from 'primeng/inputtext';
import { DefaultComponent } from './default.component';
import { DefaultRoutingModule } from './default-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { AppConfigModule } from '../../config/config.module';
import { RippleModule } from 'primeng/ripple';
import { AutoFocusModule } from 'primeng/autofocus';

@NgModule({
    imports: [
        HttpClientModule,
        AppConfigModule,
        RippleModule,
        FormsModule,
        CommonModule,
        ButtonModule,
        ReactiveFormsModule,
        AutoFocusModule,
        InputTextModule,
        DefaultRoutingModule
    ],
    declarations: [DefaultComponent]
})
export class DefaultModule { }

import { NgModule } from '@angular/core';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { AppLayoutModule } from './layout/app.layout.module';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpInterceptorService } from './layout/pages/utilites/httpInterceptor.service';
import { SpinnerComponent } from './layout/pages/utilites/spinner/spinner.component';
import { LoaderService } from './layout/pages/utilites/loaderService';

@NgModule({
    declarations: [
        AppComponent,
        SpinnerComponent
    ],
    imports: [
        AppRoutingModule,
        AppLayoutModule
    ],
    providers: [
        {provide: LoaderService},
        {provide: HTTP_INTERCEPTORS, useClass: HttpInterceptorService, multi: true},
        {provide: LocationStrategy, useClass: HashLocationStrategy}],
    bootstrap: [AppComponent]
})
export class AppModule { }
